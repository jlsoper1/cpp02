#include <iostream>
#include <fstream>
#include <string>

using std::cin;
using std::cout;
using std::string;

class Detab
{
  public:
    static const int TAB   =  9;
    static const int LF    = 10;
    static const int CR    = 13;
    static const int SPACE = 32;

  private:
    string inputFile  = "";
    string outputFile = "";

    std::ifstream streamReader;
    std::ofstream streamWriter;

    int tab     = 0;
    int tabsize = 0;

  public:
    Detab()
    {
      tabsize = 2;
    }

    void setFileNames(int argc, char* arguments[])
    {
      switch (argc)
      {
        case 2:
          tabsize = std::stoi(arguments[1]);
          inputFile = arguments[0];
          break;

        case 3:
          tabsize = std::stoi(arguments[1]);
          inputFile = arguments[2];
          break;

        default:
          cout << "Input desired tab size: ";
          cin >> tabsize;

          cout << "Input file name: ";
          cin >> inputFile;
          break;
      }

      if (!inputFile.empty())
        outputFile = inputFile + ".detab";
      else
        exit(-1);
    }

    void processFile()
    {
      int i, j;
      char c;

      streamReader.open(inputFile);
      streamWriter.open(outputFile);

      while (streamReader.get(c))
      {
        i = (int) c;

        if (i == TAB)  // if tab found, output appropriate # of spaces
        {
          for (j = tab; j < tabsize; j++)
          {
            streamWriter.put(SPACE);
          }

          tab = 0;
        }
        else
        {
          streamWriter.put(c);
          tab++;

          if (tab == tabsize)
          {
            tab = 0;
          }

          if (i == CR || i == LF)
          {
            tab = 0;
          }
        }
      }

      streamWriter.close();
      streamReader.close();
    }
};

int main(int argc, char* argv[])
{
  Detab d;

  d.setFileNames(argc, argv);
  d.processFile();

  return 0;
}
